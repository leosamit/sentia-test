# Sentia Test
This is an application for Sentia Android Code Challenge. This is mainly one screen application where list of "Properties" are displayed. The api that I used can be found here: https://demo7442132.mockable.io/test/properties

Following are the few terminologies that I have used throught the process.
1. Data binding is used to show connection between the app UI and the data
2. Architecture Pattern: MVVM
3. Database: Room
4. Network Call: Retrofit
5. Asynchronous Handling: Coroutine
6. Dependency Injection: Dagger 2
7. Image Library: Glide
8. External Library: mikhaellopez:circularimageview.

