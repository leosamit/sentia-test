package com.example.sentiaapp.ui.home

import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.example.sentiaapp.domain.model.PropertyDto
import com.example.sentiaapp.R
import com.example.sentiaapp.ui.home.adapter.HomeAdapter
import dagger.android.support.DaggerFragment
import kotlinx.android.synthetic.main.fragment_home.*
import javax.inject.Inject


class HomeFragment : DaggerFragment() {
    private val TAG: String = HomeFragment::class.java.simpleName

    companion object {
        val FRAGMENT_NAME: String = HomeFragment::class.java.name
    }

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    private val viewModel: HomeViewModel by lazy {
        ViewModelProviders.of(this, viewModelFactory).get(HomeViewModel::class.java)
    }
    val adapter: HomeAdapter by lazy { HomeAdapter(arrayListOf()) }
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_home, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        observeCall()
    }

    private fun observeCall() {
        with(viewModel) {
            homeData.observe(this@HomeFragment, Observer {
                initView(it)

            }
            )
            error.observe(this@HomeFragment, Observer {
                btn_retry.setOnClickListener {
                    //observeCall()
                    viewModel.doRetry()
                    progressBar_home.visibility = View.VISIBLE
                    refresh_home.visibility = View.GONE
                }

                progressBar_home.visibility = View.GONE
                refresh_home.visibility = View.VISIBLE
                Toast.makeText(context, "${it?.message}", Toast.LENGTH_LONG).show()


            })

            propertyCount.observe(this@HomeFragment, Observer {

            })
        }
    }

    private fun initView(it: PropertyDto?) {
        rv_main_home.layoutManager =
            androidx.recyclerview.widget.LinearLayoutManager(context, RecyclerView.VERTICAL, false)
        rv_main_home.adapter = adapter
        progressBar_home.visibility = View.GONE
        if (it!!.results.isNotEmpty()) {
            adapter.clear()
            adapter.add(it.results)

        } else {
            Toast.makeText(
                context,
                context?.getString(R.string.empty_list),
                android.widget.Toast.LENGTH_LONG
            ).show()
        }
    }


}