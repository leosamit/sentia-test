package com.example.sentiaapp.ui.home.adapter

import androidx.databinding.ViewDataBinding
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.library.baseAdapters.BR.item
import com.example.sentiaapp.databinding.PropertyItemRowBinding
import com.example.sentiaapp.domain.model.Property
import com.example.sentiaapp.ui.DataBindingViewHolder
import androidx.core.content.ContextCompat.startActivity
import android.content.Intent
import com.example.sentiaapp.ui.home.DetailActivity


class HomeAdapter(
    private var items: ArrayList<Property> = arrayListOf<Property>()
) : androidx.recyclerview.widget.RecyclerView.Adapter<HomeAdapter.SimpleHolder>() {
    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: SimpleHolder, position: Int) {
        holder.onBind(items[position])



    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SimpleHolder {
        val binding =
            PropertyItemRowBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return SimpleHolder(binding)
    }
    private fun createOnClickListener(id: Int, name: String): View.OnClickListener {
        return View.OnClickListener {

        }
    }

    inner class SimpleHolder(dataBinding: ViewDataBinding) :
        DataBindingViewHolder<Property>(dataBinding) {
        override fun onBind(t: Property): Unit = with(t) {

            dataBinding.setVariable(item, t)




        }
    }

    fun add(list: ArrayList<Property>) {
        items.addAll(list)
        notifyDataSetChanged()
    }

    fun clear() {
        items.clear()
        notifyDataSetChanged()
    }
}