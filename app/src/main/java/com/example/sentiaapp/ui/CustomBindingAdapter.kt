package com.example.sentiaapp.ui

import android.os.Build
import androidx.databinding.BindingAdapter
import android.widget.ImageView
import androidx.annotation.RequiresApi
import com.bumptech.glide.Glide
import com.squareup.picasso.Picasso
import android.widget.Toast
import androidx.core.content.ContextCompat.startActivity
import android.content.Intent
import android.R
import android.net.Uri
import androidx.cardview.widget.CardView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.constraintlayout.widget.Constraints
import com.example.sentiaapp.ui.home.DetailActivity
import com.mikhaellopez.circularimageview.CircularImageView


object CustomBindingAdapter {

//    @JvmStatic
//    @BindingAdapter("bind:image_url")
//    fun loadImage(imageView: ImageView?, url: String) {
//        Picasso.with(imageView!!.context).load(url).into(imageView)
////        Glide.with(context).load(article.getCoverImage().getUrlOriginalQ()).centerCrop()
////            .into(itemHolder.imageView)
//    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    @JvmStatic
    @BindingAdapter("bind:image_url")
    fun loadImage(imageView: CircularImageView?, url: String) {
        Glide.with(imageView!!.context)
            .load(url)
            .centerCrop()
            .crossFade()
            .placeholder(imageView.context.getDrawable(com.example.sentiaapp.R.mipmap.placeholder_profile))
            .dontAnimate()
            .into(imageView)

    }
    //https://www.shareicon.net/data/128x128/2016/01/12/702155_users_512x512.png"
    //https://storage.googleapis.com/idx-photos-gs.ihouseprd.com/TX-NTREIS/13807656/org/003.jpg"

    @JvmStatic
    @BindingAdapter("bind:image_urls")
    fun loadImages(imageView: ImageView?, url: String) {
        Glide.with(imageView!!.context)
            .load(url)
            .crossFade()
            .into(imageView)

    }
    //app:itemOnClick="@{ item.id }"


    @JvmStatic
    @BindingAdapter("bind:itemOnClick")
    fun bindItemClickListener(imageButton: CardView, id: String?) {
        imageButton.setOnClickListener { view ->
            if (id != null) {
                val context = imageButton.context
                val intent = Intent(context, DetailActivity::class.java)
                intent.putExtra("id", id)
                context.startActivity(intent)


            }
        }
    }


}