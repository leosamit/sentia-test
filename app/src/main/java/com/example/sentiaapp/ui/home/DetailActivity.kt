package com.example.sentiaapp.ui.home

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.sentiaapp.R
import kotlinx.android.synthetic.main.activity_detail.*
import kotlinx.android.synthetic.main.toolbar_main.*


class DetailActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)
        tv_id.text = "Id: " + intent.getStringExtra("id")
        setSupportActionBar(toolbar_main_activity)

        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowHomeEnabled(true)
        supportActionBar!!.setDisplayShowTitleEnabled(false)


        toolbar_main_activity.setNavigationOnClickListener {
            onBackPressed()
        }


    }
}
