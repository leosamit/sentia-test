package com.example.sentiaapp.ui.home

import androidx.lifecycle.MutableLiveData
import android.util.Log
import androidx.lifecycle.ViewModel
import com.example.sentiaapp.domain.usecase.GetHomeUseCase
import com.example.sentiaapp.domain.model.PropertyDto
import com.example.sentiaapp.domain.model.response.ErrorModel
import com.example.sentiaapp.domain.model.response.ErrorStatus
import com.example.sentiaapp.domain.usecase.GetAllPropertyUseCase
import com.example.sentiaapp.domain.usecase.InsertPropertyUseCase
import javax.inject.Inject
import android.icu.lang.UCharacter.GraphemeClusterBreak.T
import androidx.lifecycle.MediatorLiveData


class HomeViewModel @Inject constructor(
    private val getHomeUseCase: GetHomeUseCase,
    private val insertPropertyUseCase: InsertPropertyUseCase,
    private val getAllPropertyUseCase: GetAllPropertyUseCase
) : ViewModel() {
    private val TAG = HomeViewModel::class.java.simpleName
    val homeData: MutableLiveData<PropertyDto> by lazy { MutableLiveData<PropertyDto>() }
    val error: MutableLiveData<ErrorModel> by lazy { MutableLiveData<ErrorModel>() }
    val propertyCount: MutableLiveData<Int> by lazy { MutableLiveData<Int>() }

    private val mRetry = false



    init {
        executeHomeUseCase()
    }
    public  fun executeHomeUseCase(){



        getHomeUseCase.execute {
            onComplete {

                homeData.value = it
                insert(it)
            }

            onError { throwable ->
                if (throwable.errorStatus == ErrorStatus.UNAUTHORIZED) {
                    doReshresh()
                } else {
                    error.value = throwable
                }

            }

            onCancel {

            }
        }
    }

    private fun doReshresh() {

    }

    public fun doRetry() {
        //mRetry.setValue(true)
       executeHomeUseCase()


    }


    fun insert(propertyDto: PropertyDto) {

        insertPropertyUseCase.propertyDto = propertyDto
        insertPropertyUseCase.execute {

            onComplete {
                returnPropertyInDb()
            }

            onError { throwable ->
                error.value = throwable
            }

            onCancel {

            }
        }
    }

    private fun returnPropertyInDb() {
        getAllPropertyUseCase.execute {
            onComplete {
                propertyCount.value = it.size
            }
            onError {
                error.value = it
            }
        }
    }

    override fun onCleared() {
        super.onCleared()
        insertPropertyUseCase.unsubscribe()
        getHomeUseCase.unsubscribe()
        getAllPropertyUseCase.unsubscribe()
    }
}