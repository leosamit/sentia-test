package com.example.sentiaapp.data.repository

import com.example.sentiaapp.data.source.cloud.BaseCloudRepository
import com.example.sentiaapp.data.source.db.PropertyDao
import com.example.sentiaapp.domain.model.Property
import com.example.sentiaapp.domain.model.PropertyDto
import com.example.sentiaapp.domain.model.PropertyResponse
import com.example.sentiaapp.domain.model.dao.Response
import com.example.sentiaapp.domain.repository.AppRepository
import javax.inject.Inject

class AppRepoImp @Inject constructor(
    private val cloudRepository: BaseCloudRepository,
    private val propertyDao: PropertyDao
) : AppRepository {
    override suspend fun selectAllProperty(): MutableList<Property> {

        return propertyDao.selectProperty()

    }

    override suspend fun saveProperty(propertyDto: PropertyDto): Long {
        if (propertyDto.results.size > 0) {
            for (property in propertyDto.results) {
                propertyDao.insertPropert(property)
            }

        }
        return 0L
    }

    override suspend fun getHome(): PropertyDto {
        return cloudRepository
            .getHome().data
    }


}