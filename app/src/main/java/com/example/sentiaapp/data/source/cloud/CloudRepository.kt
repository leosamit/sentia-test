package com.example.sentiaapp.data.source.cloud


import android.util.Log
import com.example.sentiaapp.data.restful.ApiService
import com.example.sentiaapp.domain.model.PropertyDto
import com.example.sentiaapp.domain.model.PropertyResponse
import com.example.sentiaapp.domain.model.dao.Response
import com.google.gson.Gson

class CloudRepository(private val apIs: ApiService) : BaseCloudRepository {
    override suspend fun getHome(): PropertyResponse {
        return apIs.getHome().await()
    }

}
