package com.example.sentiaapp.data.restful

import com.example.sentiaapp.domain.model.PropertyDto
import com.example.sentiaapp.domain.model.PropertyResponse
import com.example.sentiaapp.domain.model.dao.Response
import kotlinx.coroutines.Deferred
import retrofit2.Call
import retrofit2.http.GET

interface ApiService {

//    @GET("properties/")
//    fun getHome()
//            : Deferred<PropertyDto>

    @GET("properties")
    fun getHome()
            : Deferred<PropertyResponse>


}