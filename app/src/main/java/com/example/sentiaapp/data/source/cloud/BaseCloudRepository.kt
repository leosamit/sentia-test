package com.example.sentiaapp.data.source.cloud

import com.example.sentiaapp.domain.model.PropertyDto
import com.example.sentiaapp.domain.model.PropertyResponse
import com.example.sentiaapp.domain.model.dao.Response


interface BaseCloudRepository {
   suspend fun getHome(): PropertyResponse
}