package com.example.sentiaapp.data.source.db

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.example.sentiaapp.domain.model.Converters
import com.example.sentiaapp.domain.model.Property

@Database(entities = [Property::class], version = AppDatabase.VERSION, exportSchema = false)
@TypeConverters(Converters::class)

abstract class AppDatabase : RoomDatabase() {
    companion object {
        const val DB_NAME = "Property.db"
        const val VERSION = 1
    }

    abstract fun propertyDao(): PropertyDao
}