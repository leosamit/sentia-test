package com.example.sentiaapp.data.source.db

import androidx.room.*
import com.example.sentiaapp.domain.model.Property


@Dao
interface PropertyDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertPropert(property: Property): Long

    @Delete
   suspend fun deleteProperty(property: Property): Int

    @Query("SELECT * from Property")
   suspend fun selectProperty(): MutableList<Property>

}