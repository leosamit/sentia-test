package com.example.sentiaapp.domain.model.dao

data class Response(
    val ad_id: Int,
    val `data`: Data,
    val title: String
)