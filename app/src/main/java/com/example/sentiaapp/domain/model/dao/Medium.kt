package com.example.sentiaapp.domain.model.dao

import androidx.room.ColumnInfo
import androidx.room.Embedded
import com.google.gson.annotations.SerializedName

data class Medium(

    @SerializedName("url")
    val url: String?
)