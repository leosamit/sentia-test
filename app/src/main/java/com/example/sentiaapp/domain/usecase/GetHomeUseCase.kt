package com.example.sentiaapp.domain.usecase

import com.example.sentiaapp.data.mapper.CloudErrorMapper
import com.example.sentiaapp.domain.model.PropertyDto
import com.example.sentiaapp.domain.model.dao.Response
import com.example.sentiaapp.domain.repository.AppRepository
import com.example.sentiaapp.domain.use.UseCase
import javax.inject.Inject

class GetHomeUseCase @Inject constructor(
    errorUtil: CloudErrorMapper,
    private val appRepository: AppRepository
) : UseCase<PropertyDto>(errorUtil) {
    override suspend fun executeOnBackground(): PropertyDto {
        return appRepository.getHome()
    }


}