package com.example.sentiaapp.domain.model.dao

import android.os.Parcel
import android.os.Parcelable
import androidx.room.Embedded
import androidx.room.Entity
import com.google.gson.annotations.SerializedName


data class Image(

    @SerializedName("medium")
    @Embedded
    val medium: Medium?

)

//):Parcelable {
//    constructor(parcel: Parcel) : this(
//        TODO("big"),
//        TODO("medium"),
//        TODO("small")
//    ) {
//    }
//
//    override fun writeToParcel(p0: Parcel?, p1: Int) {
//        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
//    }
//
//    override fun describeContents(): Int {
//        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
//    }
//
//    companion object CREATOR : Parcelable.Creator<Image> {
//        override fun createFromParcel(parcel: Parcel): Image {
//            return Image(parcel)
//        }
//
//        override fun newArray(size: Int): Array<Image?> {
//            return arrayOfNulls(size)
//        }
//    }
//}