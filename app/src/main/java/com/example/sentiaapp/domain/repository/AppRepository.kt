package com.example.sentiaapp.domain.repository

import com.example.sentiaapp.domain.model.Property
import com.example.sentiaapp.domain.model.PropertyDto
import com.example.sentiaapp.domain.model.PropertyResponse
import com.example.sentiaapp.domain.model.dao.Response

interface AppRepository {
    suspend fun getHome(): PropertyDto
    suspend fun saveProperty(propertyDto: PropertyDto): Long
    suspend fun selectAllProperty(): MutableList<Property>
}