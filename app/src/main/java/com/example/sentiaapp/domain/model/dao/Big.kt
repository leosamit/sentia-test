package com.example.sentiaapp.domain.model.dao

import androidx.room.Embedded
import com.google.gson.annotations.SerializedName

data class Big(
    @SerializedName("url")
    @Embedded
    val url: String
)