package com.example.sentiaapp.domain.usecase

import com.example.sentiaapp.data.mapper.CloudErrorMapper
import com.example.sentiaapp.domain.model.PropertyDto
import com.example.sentiaapp.domain.repository.AppRepository
import com.example.sentiaapp.domain.use.UseCase
import javax.inject.Inject

class InsertPropertyUseCase @Inject constructor(
    errorUtil: CloudErrorMapper,
    private val appRepository: AppRepository
) : UseCase<Long>(errorUtil) {
    var propertyDto= PropertyDto(arrayListOf())
    override suspend fun executeOnBackground(): Long {
        return appRepository.saveProperty(propertyDto)
    }

}