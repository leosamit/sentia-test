package com.example.sentiaapp.domain.model.dao

data class Listings(
    val AgencyLogoUrl: String,
    val Area: String,
    val AuctionDate: String,
    val AvailableFrom: Any,
    val Bathrooms: Int,
    val Bedrooms: Int,
    val Carspaces: Int,
    val Currency: String,
    val DateFirstListed: String,
    val DateUpdated: String,
    val Description: String,
    val DisplayPrice: String,
    val Id: String,
    val ImageUrls: List<String>,
    val IsPriority: Int,
    val Latitude: Double,
    val ListingPrice: Any,
    val ListingStatistics: Any,
    val ListingType: String,
    val ListingTypeString: String,
    val Location: Location,
    val Longitude: Double,
    val is_premium: Int,
    val owner: Owner
)