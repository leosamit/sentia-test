package com.example.sentiaapp.domain.model.dao

import com.example.sentiaapp.domain.model.Property
import com.google.gson.annotations.SerializedName

data class Data(
    @SerializedName("listings") val results: ArrayList<Property>

    //val listings: List<Listings>
)