package com.example.sentiaapp.domain.model


import androidx.room.Entity
import androidx.room.PrimaryKey
import android.os.Parcel
import android.os.Parcelable
import androidx.room.Embedded
import com.example.sentiaapp.domain.model.dao.Image
import com.example.sentiaapp.domain.model.dao.Location
import com.example.sentiaapp.domain.model.dao.Owner
import com.google.gson.annotations.SerializedName
import org.jetbrains.annotations.NotNull


@Entity
data class Property(
    //@PrimaryKey(autoGenerate = true)
    @PrimaryKey
    @NotNull
    @SerializedName("Id") var id: String,
    @SerializedName("Area") var title: String?,
    @SerializedName("AgencyLogoUrl") var agencyLogoUrl: String?,
    @SerializedName("Bathrooms") val bathRooms: Int?,
    @SerializedName("Carspaces") val carSpaces: Int?,
    @SerializedName("Bedrooms") val bedRooms: Int?,
    @SerializedName("Location")
    @Embedded
    val location: Location?,
    @SerializedName("owner")
    @Embedded
    val owner: Owner?,

    @SerializedName("ImageUrls")
    val propertyImages: List<String>?


//    @SerializedName("ImageUrls")
//    @Embedded
//    val imageUrls: List<String>


)
//@SerializedName("Location") val location: Location

// @SerializedName("Owner") val owner: Owner


//) : Parcelable {
//    constructor(source: Parcel) : this(
//        source.readString()!!,
//        source.readString()!!,
//        source.readString()!!,
//        source.readString()!!,
//        source.readString()!!,
//        source.readInt(),
//        source.readInt(),
//        source.readInt()
//       //source.readParcelable<Owner>(Owner::class.java!!.classLoader)!!
//
//    )
//
//    override fun describeContents() = 0
//
//    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
//        writeString(id)
//        writeString(title)
//        writeString(href)
//        writeString(ingredients)
//        writeString(thumbnail)
//        writeInt(bathRooms)
//        writeInt(carSpaces)
//        writeInt(bedRooms)
//        //writeParcelable(owner,11)
//    }
//
//    companion object {
//        @JvmField
//        val CREATOR: Parcelable.Creator<Property> = object : Parcelable.Creator<Property> {
//            override fun createFromParcel(source: Parcel): Property = Property(source)
//            override fun newArray(size: Int): Array<Property?> = arrayOfNulls(size)
//        }
//    }
//}