package com.example.sentiaapp.domain.model

import com.example.sentiaapp.domain.model.dao.Data
import com.google.gson.annotations.SerializedName

data class PropertyDto(

    @SerializedName("listings") val results: ArrayList<Property>

)