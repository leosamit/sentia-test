package com.example.sentiaapp.domain.model.dao

import android.os.Parcel
import android.os.Parcelable
import androidx.room.Embedded
import androidx.room.Entity
import com.google.gson.annotations.SerializedName


data class Owner(
    @SerializedName("dob")
    val dob: String,
    @SerializedName("image")
    @Embedded
    val image: Image,
    @SerializedName("lastName")
    val lastName: String,
    @SerializedName("name")
    val name: String
)
//) : Parcelable {
//    constructor(parcel: Parcel) : this(
//        parcel.readString()!!,
//        parcel.readParcelable<Image>(Image::class.java!!.classLoader)!!,
//        parcel.readString()!!,
//        parcel.readString()!!
//    )
//
//    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
//        writeString(dob)
//        writeParcelable(image, 1)
//        writeString(lastName)
//        writeString(name)
//    }
//
//    override fun describeContents(): Int {
//        return 0
//    }
//
//
//    companion object CREATOR : Parcelable.Creator<Owner> {
//        override fun createFromParcel(parcel: Parcel): Owner {
//            return Owner(parcel)
//        }
//
//        override fun newArray(size: Int): Array<Owner?> {
//            return arrayOfNulls(size)
//        }
//    }
//}