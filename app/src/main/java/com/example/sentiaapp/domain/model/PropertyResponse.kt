package com.example.sentiaapp.domain.model

import com.example.sentiaapp.domain.model.dao.Data
import com.google.gson.annotations.SerializedName

data class PropertyResponse(
    @SerializedName("ad_id")
    val ad_id: Int,
    @SerializedName("data")
    val `data`: PropertyDto,

    @SerializedName("title")
    val title: String
)