package com.example.sentiaapp.domain.model.dao

import androidx.room.Entity
import com.google.gson.annotations.SerializedName


data class Location(
    @SerializedName("Address")
    val address: String,
    @SerializedName("Address2")
    val address2: String,
    @SerializedName("State")
    val state: String,
    @SerializedName("Suburb")
    val suburb: String
)