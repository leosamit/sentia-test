package com.example.sentiaapp.domain.usecase

import com.example.sentiaapp.domain.model.Property
import com.example.sentiaapp.data.mapper.CloudErrorMapper
import com.example.sentiaapp.domain.repository.AppRepository
import com.example.sentiaapp.domain.use.UseCase
import javax.inject.Inject

class GetAllPropertyUseCase @Inject constructor(
    errorUtil: CloudErrorMapper,
    private val appRepository: AppRepository
) : UseCase<MutableList<Property>>(errorUtil) {
    override suspend fun executeOnBackground(): MutableList<Property> {
        return appRepository.selectAllProperty()
    }
}