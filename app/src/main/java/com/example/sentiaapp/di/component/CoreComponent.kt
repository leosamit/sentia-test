package com.example.sentiaapp.di.component

import android.app.Application
import com.example.sentiaapp.core.App
import com.example.sentiaapp.di.builder.ActivityBuilder
import com.example.sentiaapp.di.module.ContextModule
import com.example.sentiaapp.di.module.DataBaseModule
import com.example.sentiaapp.di.module.NetworkModule

import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjector
import javax.inject.Singleton
import dagger.android.support.AndroidSupportInjectionModule

@Singleton
@Component(modules = [AndroidSupportInjectionModule::class, NetworkModule::class, ActivityBuilder::class, ContextModule::class, DataBaseModule::class])
interface CoreComponent : AndroidInjector<App> {

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): Builder

        fun build(): CoreComponent
    }


}