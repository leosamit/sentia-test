package com.example.sentiaapp.di.module

import android.app.Application
import androidx.room.Room
import com.example.sentiaapp.data.source.db.AppDatabase
import com.example.sentiaapp.data.source.db.PropertyDao
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class DataBaseModule {

    @Provides
    @Singleton
    fun provideRoomDatabase(application: Application): AppDatabase {
        return Room
                .databaseBuilder(application, AppDatabase::class.java, AppDatabase.DB_NAME)
                .fallbackToDestructiveMigration()
                .build()
    }

    @Provides
    fun provideUserDao(appDataBase: AppDatabase): PropertyDao {
        return appDataBase.propertyDao()
    }
}