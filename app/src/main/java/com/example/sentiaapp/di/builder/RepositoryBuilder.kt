package com.example.sentiaapp.di.builder


import com.example.sentiaapp.data.repository.AppRepoImp
import com.example.sentiaapp.domain.repository.AppRepository
import dagger.Binds
import dagger.Module

@Module
abstract class RepositoryBuilder {
    @Binds
    abstract fun bindsMovieRepository(repoImp: AppRepoImp): AppRepository
}