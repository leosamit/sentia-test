package com.example.sentiaapp.di.builder

import androidx.lifecycle.ViewModelProvider
import com.example.sentiaapp.di.builder.AppViewModelBuilder
import com.example.sentiaapp.di.builder.RepositoryBuilder
import com.example.sentiaapp.ui.ViewModelFactory
import dagger.Binds
import dagger.Module

@Module(includes = [
    (RepositoryBuilder::class),
    (AppViewModelBuilder::class)
])
abstract class ViewModelBuilder {

    @Binds
    abstract fun bindViewModelFactory(viewModelFactory: ViewModelFactory): ViewModelProvider.Factory
}